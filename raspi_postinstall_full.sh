#!/bin/bash

#Une série de scripts conçue pour faciliter la configuration du Raspberry Pi après une "fresh install". :-)

main(){

	ROUGEFONCE='\e[0;31m'
	VERTFONCE='\e[0;32m'
	NEUTRE='\e[0;m'

	sleep 1

	echo -e """
	${VERTFONCE}@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	@                                                           @
	@       ${ROUGEFONCE}SCRIPTS DE POST-INSTALLATION POUR RASPBERRY PI      ${VERTFONCE}@
	@                                                           @
	@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@${NEUTRE}
	
	${ROUGEFONCE}        /!\ Certaines opérations nécessitent sudo ! /!\ ${NEUTRE}	
	"""

	echo -e "Que voulez-vous faire ?\n"
	echo -e "\t[1] Passer le clavier en français"
	echo -e "\t[2] Installer Vim"
	echo -e "\t[3] Ajouter un nouvel utilisateur"
	echo -e "\t[4] Supprimer l'utilisateur pi"
	echo -e "\t[5] Paramétrer le Wi-Fi"
	echo -e "\t[6] Paramétrer le Wi-Fi (SD)"
	echo -e "\t[7] Activer SSH (SD)"
	echo -e "\t[8] Configurer l'écran"
	echo -e "\t[9] Configurer l'écran (SD)"
	echo -e "\t[10] Installer la fonction écran tactile"
	echo -e "\t[11] Installer le PiJuice"
	echo -e "\t[12] Installer le SenseHat"
	echo -e "\t[13] Lancer raspi-config"
	echo -e "\t[14] Quitter ce programme\n"
	echo "Votre choix :" 
	read REPONSE

	case $REPONSE in
        	1)
                	keyboard_fr #appel fonction
			main #appel fonction
                	;;
	                
	        2)
	                vim_install #appel fonction
			main #appel fonction
	                ;;
		3)
	                add_user #appel fonction
			main #appel fonction
	                ;;
	        4)      
			deluser_pi #appel fonction
			main #appel fonction
	                ;;
	        5)
	                wpa_supplicant_raspi #appel fonction
			main #appel fonction
	                ;;
	        6)
	                wpa_supplicant_sd #appel fonction
			main #appel fonction
	                ;;
	        7)
	                ssh_activation_sd #appel fonction
			main #appel fonction
	                ;;
	        8)
	                Waveshare_5_raspi #appel fonction
			main #appel fonction
	             	;;
	        9)
	                Waveshare_5_sd #appel fonction
			main #appel fonction
	                ;;
	        10)
	                LCD-show #appel fonction
			main #appel fonction
	                ;;
	        11)
	                PiJuice #appel fonction
			main #appel fonction
	                ;;
	        12)
	                SenseHat #appel fonction
			main #appel fonction
	                ;;
	        13)
	                sudo raspi-config
	                ;;
	        14)
	                echo -e "\nFin du programme.\n"
	                sleep 1
	                ;;
	        *)
	                echo -e "\nMauvaise réponse ! Same player, shoot again!\n"
			sleep 1
			main #appel fonction
        	        ;;
	esac

}

#Les autres fonctions sont classées par ordre alphabétique

add_groups(){

        #Ajout des groupes administrateur au nouvel utilisateur :
	echo "Ajouts des groupes adm, dialout, cdrom, sudo, audio, video, plugdev, games, users, input, netdev, gpio, i2c et spi à l'utilisateur $NOM..."
        sleep 1
        sudo usermod -aG adm,dialout,cdrom,sudo,audio,video,plugdev,games,users,input,netdev,gpio,i2c,spi $NOM && echo -e "\n$NOM a bien été ajouté aux groupes sus-nommés.\n"
        sleep 1

        if [ $(cat /etc/group | grep bluetooth | cut -d: -f1) = "bluetooth" ]
        then
                echo "Ajout du groupe bluetooth à l'utilisateur $NOM..."
                sleep 1
                sudo gpasswd -a $NOM bluetooth && echo -e "\n$NOM a bien été ajouté au groupe bluetooth."
                sleep 1
        fi

}

add_user(){
	
	#Ajout d'un nouvel utilisateur :
	echo -e "\nQuel nom pour le nouvel utilisateur ?"
	read NOM
	echo -e "\nAjout de l'utilisateur $NOM..."
	sleep 1
	sudo adduser $NOM && echo -e "\nL'utilisateur $NOM a bien été ajouté."
	sleep 1

	#Ajout des groupes administrateur au nouvel utilisateur :
	echo -e "\nVoulez-vous accorder tous les droits d'administration à l'utilisateur $NOM ? (o/n)"
	read REPONSE
	if [ $REPONSE = "o" ]
	then
		add_groups #appel fonction
	fi
	echo -e "\nUn redémarrage est nécessaire pour supprimer pi. Veuillez vous logger avec le nouvel utilisateur pour ce faire."
	sleep 1

	#Configuration de sudo sans mot de passe :
	echo -e "\nVoulez-vous pouvoir exécuter sudo sans mot de passe ? (o/n)"
	read REPONSE
	if [ $REPONSE = "o" ]
	then
		sudo_config #appel fonction
	fi

}

copy_pi(){

	#Sauvegarde de /home/pi :
	echo -e "\nChemin de l'emplacement où copier ?"
	read CHEMIN
	echo -e "\nCopie de /home/pi vers $CHEMIN..."
	sleep 1
	sudo rsync -a /home/pi $CHEMIN && echo -e "\n/home/pi a bien été copié dans $CHEMIN.\n"
	sleep 1

}

deluser_pi(){
 
	#Sauvegarde du dossier /home/pi :
	echo -e "\nAvant de commencer, voulez-vous copier le contenu du home de pi à un autre emplacement ? (o/n)"
        read REPONSE
        if [ $REPONSE = "o" ]
        then
                copy_pi #appel fonction
        fi

	#Suppression de l'utilisateur pi :
	echo -e "\nSuppression de l'utilisateur pi..."
	sleep 1
	sudo deluser --remove-home pi && echo -e "\nL'utilisateur pi a bien été supprimé.\n"
	sleep 1

}
	

keyboard_fr(){

	#Paramétrage du clavier en français avec setxkbmap :
	echo -e "\nPassage du clavier en français..."
	sleep 1
	setxkbmap fr && echo -e "\nLe clavier est bien paramétré pour le français.\n"
	sleep 1

}

LCD-show(){

	#Installation de LCD-show pour activer la fonction écran tactile :
	echo -e "\nInstallation de LCD-show..."
	sleep 1
	tar xzvf /boot/LCD-show-*.tar.gz
	cd LCD-show/
	chmod +x LCD5-show
	sudo ./LCD5-show && echo -e "\nInstallation de LCD-show réussie.\n"
	sleep 1

}

PiJuice(){

	#Options d'installation du PiJuice:
	echo -e "\nQuelle version du logiciel PiJuice souhaitez-vous installer ?\n"
	echo -e "\t[1] La version avec GUI"
	echo -e "\t[2] La version light (sans interface graphique)"
	echo -e "\t[3] Aucune, revenir au programme principal\n"
	echo "Votre choix :"
	read REPONSE

	case $REPONSE in
        	1)
                	#Installation du PiJuice avec GUI :
			echo -e "\nInstallation de la version avec GUI..."
                	sleep 1
                	sudo apt install -y pijuice-gui && echo -e "\nPiJuice avec GUI a été correctement installé.\n"
                	sleep 1
                	;;
        	2)
                	#Installation du PiJuice sans GUI :
			echo -e "\nInstallation de la version light..."
                	sleep 1
                	sudo apt install -y pijuice-base && echo -e "\nPiJuice light a été correctement installé.\n"
                	sleep 1
                	;;
        	3)
                	echo -e "\nRetour au script de post-installation...\n"
                	sleep 1
                	;;
       	 	*)
                	echo -e "\nYou lose!\n"
                	PiJuice #appel fonction
                	;;
	esac

}

SenseHat(){

	#Installation du SenseHat :
	echo -e "\nInstallation du SenseHat..."
	sleep 1
	sudo apt install -y sense-hat && echo -e "\nLe SenseHat a été correctement installé.\nUn redémarrage peut être nécessaire pour que les changements prennent effet.\n"
	sleep 1

}

ssh_activation_sd(){

	#Création d'un fichier ssh vide dans la partition boot de la carte SD:
	echo -e "\nCréation d'un fichier ssh vide dans la partition /boot..."
	sleep 1
	> /media/$USER/boot/ssh && echo -e "\nLe fichier a bien été créé, ssh sera activé au boot.\n"
	sleep 1

}

sudo_config(){

	#paramétrage du sudo sans mot de passe via un fichier pour le nouvel utilisateur situé dans /etc/sudoers.d :
	echo "Configuration du fichier /etc/sudoers.d/010_$NOM-nopasswd..."
        sudo su
	sleep 1
        echo "$NOM ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/010_$NOM-nopasswd && exit && echo -e "\nsudo a été correctement configuré pour l'utilisateur $NOM.\nUn redémarrage de session est nécessaire pour que le changement prenne effet.\n"
        sleep 1

}

vim_install(){

	#Installation de Vim :
	echo -e "\nInstallation de Vim..."
	sleep 1
	sudo apt install -y vim && echo -e "\nVim a été correctement installé.\n"
	sleep 1
}

Waveshare_5_raspi(){

	#Réglage de la résolution d'écran pour le Waveshare 5 pouces via /boot/config.txt : 
	echo -e "\nSauvegarde de config.txt..."
	sleep 1
	sudo cp /boot/config.txt /boot/config.txt.bak && echo "config.txt a été correctement sauvegardé."
	sleep 1
	echo -e "\nCopie des paramètres de l'écran Waveshare LCD 5 pouces à la fin de config.txt..."
	sleep 1
	sudo echo """max_usb_current=1
	hdmi_group=2
	hdmi_mode=87
	hdmi_cvt 800 480 60 6 0 0 0
	hdmi_drive=1""" >> /boot/config.txt && echo -e "\nLes paramètres ont été correctement copiés."
	sleep 1
	
	#Installation de LCD-show :
	echo -e "\nVoulez-vous activer la fonction écran tactile ? (o/n)"
	read REPONSE	
	if [ $REPONSE = "o" ]
	then
		LCD-show #appel fonction
	else
		echo -e "\nRetour au menu principal...\n"
		sleep 1
	fi

}

Waveshare_5_sd(){

	#Réglage de la résolution d'écran pour le Waveshare 5 pouces via /boot/config.txt : 
        echo -e "\nSauvegarde de config.txt..."
        sleep 1
        cp /media/$USER/boot/config.txt /media/$USER/boot/config.txt.bak && echo "config.txt a été correctement sauvegardé."
        sleep 1
        echo -e "\nCopie des paramètres de l'écran Waveshare LCD 5 pouces à la fin de config.txt..."
        sleep 1
        echo """max_usb_current=1
        hdmi_group=2
        hdmi_mode=87
        hdmi_cvt 800 480 60 6 0 0 0
        hdmi_drive=1""" >> /media/$USER/boot/config.txt && echo -e "\nLes paramètres ont été correctement copiés.\n"
        sleep 1

}

wpa_supplicant_raspi(){
	
	#Activation des réseaux wifi via le fichier /etc/wpa_supplicant/wpa_supplicant.conf :
	echo -e "\n Sauvegarde de /etc/wpa_supplicant/wpa_supplicant.conf..."
	sleep 1
	sudo cp /etc/wpa_supplicant/wpa_supplicant.conf /etc/wpa_supplicant/wpa_supplicant.conf.bak && echo "Le fichier /etc/wpa_supplicant.conf original a été correctement sauvegardé"
	sleep 1
	echo -e "\nCopie du fichier wpa_supplicant.conf pré-configuré..."
	sleep 1
	sudo cp wpa_supplicant.conf /etc/wpa_supplicant/wpa_supplicant.conf && echo -e "\nLe Wi-Fi a été correctement configuré.\n"
	sleep 1

}

wpa_supplicant_sd(){

	#Activation des réseaux wifi via le fichier /etc/wpa_supplicant/wpa_supplicant.conf :
        echo -e "\n Sauvegarde de /etc/wpa_supplicant/wpa_supplicant.conf..."
        sleep 1
        sudo cp /media/$USER/rootfs/etc/wpa_supplicant/wpa_supplicant.conf /media/$USER/rootfs/etc/wpa_supplicant/wpa_supplicant.conf.bak && echo "Le fichier /etc/wpa_supplicant.conf original a été correctement sauvegardé"
        sleep 1
        echo -e "\nCopie du fichier wpa_supplicant.conf pré-configuré..."
        sleep 1
        sudo cp wpa_supplicant.conf /media/$USER/rootfs/etc/wpa_supplicant/wpa_supplicant.conf && echo -e "\nLe Wi-Fi a été correctement configuré.\n"
        sleep 1

}

main #appel fonction
