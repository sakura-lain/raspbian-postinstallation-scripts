#! /bin/bash

echo "\nSauvegarde de config.txt..."
sleep 1
cp /media/$USER/boot/config.txt /media/$USER/boot/config.txt.bak && echo "config.txt a été correctement sauvegardé."
sleep 1
echo "\nCopie des paramètres de l'écran Waveshare LCD 5 pouces à la fin de config.txt..."
sleep 1
echo """max_usb_current=1
hdmi_group=2
hdmi_mode=87
hdmi_cvt 800 480 60 6 0 0 0
hdmi_drive=1""" >> /media/$USER/boot/config.txt && echo "Les paramètres ont été correctement copiés."
sleep 1
