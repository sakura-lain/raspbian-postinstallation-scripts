#!/bin/bash

echo "Quel nom pour le nouvel utilisateur ?"
read NOM
echo "Ajout de l'utilisateur $NOM..."
sleep 1
adduser $NOM && echo "L'utilisateur $NOM a bien été ajouté."
sleep 1
echo "Voulez-vous accorder tous les droits d'administration à l'utilisateur $NOM ? (o/n)"
read REPONSE
if [ $REPONSE = "o" ]
then
	echo "Ajouts des groupes adm, dialout, cdrom, sudo, audio, video, plugdev, games, users, input, netdev, gpio, i2c et spi à l'utilisateur $NOM..."
	sleep 1
	useradd -G adm,dialout,cdrom,sudo,audio,video,plugdev,games,users,input,netdev,gpio,i2c,spi $NOM && echo "$NOM a bien été ajouté aux groupes sus-nommés."
	sleep 1
	if [ $(cat /etc/group | grep bluetooth | cut -d: -f1) = "bluetooth" ]
	then
		echo "Ajout du groupe bluetooth à l'utilisateur $NOM..."
		sleep 1
		useradd -G bluetooth $NOM && echo "$NOM a bien été ajouté au groupe bluetooth."
		sleep 1
	fi
	echo "Un redémarrage est nécessaire pour supprimer pi. Veuillez vous logger avec le nouvel utilisateur pour ce faire."
	sleep 1
fi
echo "Voulez-vous pouvoir exécuter sudo sans mot de passe ? (o/n)"
read REPONSE
if [ $REPONSE = "o" ]
then
	echo "Configuration du fichier /etc/sudoers/010_$NOM-nopasswd..."
	sleep 1
	echo "$NOM ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/010_$NOM-nopasswd && echo -e "sudo a été correctement configuré pour l'utilisateur $NOM.\nUn redémarrage de session est nécessaire pour que le changement prenne effet."
	sleep 1
fi
