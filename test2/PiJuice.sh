#! /bin/bash

echo -e "Quelle version du logiciel PiJuice souhaitez-vous installer ?\n"
echo -e "\t[1] La version avec GUI"
echo -e "\t[2] La version light (sans interface graphique)"
echo -e "\t[3] Aucune, revenir au programme principal\n"
echo "Votre choix :"
read REPONSE
case $REPONSE in
	1)
		echo "Installation de la version avec GUI..."
		sleep 1
		apt install -y pijuice-gui && echo "PiJuice avec GUI a été correctement installé."
		sleep 1
		;;
	2)
		echo "Installation de la version light..."
		sleep 1
		apt install -y pijuice-base && echo "PiJuice light a été correctement installé."
		sleep 1
		;;
	3)
		echo "Retour au script de post-installation...\n"
		sleep 1
		;;
	*)
		echo "You lose!\n"
		exec $0
		;;
esac
