#!/bin/bash
#Copie les fichiers et dossiers du répertoire courant vers un autre répaertoire


ROUGEFONCE='\e[0;31m'
NEUTRE='\e[0;m'

echo -e """
${ROUGEFONCE}Ce script opie les fichiers et dossiers du répertoire courant vers un autre répertoire.${NEUTRE}

Chemin du répertoire de destination (le créer s'il n'existe pas) ?
"""
read FOLDER

if [ ! -e $FOLDER ]
then
	mkdir $FOLDER
fi

for i in $(ls "$PWD")
do
	rsync -a --progress --exclude "$FOLDER" $i $FOLDER
done
