#!/bin/bash
#Copie les fichiers du répertoire courant les uns à la suite des autres dans un unique fichier.


ROUGEFONCE='\e[0;31m'
NEUTRE='\e[0;m'

echo -e """
${ROUGEFONCE}Ce script copie les fichiers du répertoire courant les uns à la suite des autres dans un unique fichier.${NEUTRE}

Nom du fichier à créer ?
"""
read FILE

for i in $(ls "$PWD")
do
	echo """$(cat $i)""" >> $FILE	
done
