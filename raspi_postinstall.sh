#!/bin/bash

ROUGEFONCE='\e[0;31m'
VERTFONCE='\e[0;32m'
NEUTRE='\e[0;m'

sleep 1

echo -e """
${VERTFONCE}@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
@                                              		    @
@	${ROUGEFONCE}Scripts de post-installation pour Raspberry Pi 	    ${VERTFONCE}@
@                                              		    @
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@${NEUTRE}
"""

echo -e "Que voulez-vous faire ?\n"
echo -e "\t[1] Passer le clavier en français"
echo -e "\t[2] Ajouter un nouvel utilisateur"
echo -e "\t[3] Supprimer l'utilisateur pi"
echo -e "\t[4] Paramétrer le Wi-Fi"
echo -e "\t[5] Installer la fonction touch screen"
echo -e "\t[6] Installer le PiJuice"
echo -e "\t[7] Installer le SenseHat"
echo -e "\t[8] Lancer raspi-config"
echo -e "\t[9] Quitter ce programme\n"
echo "Votre choix :"
read REPONSE

case $REPONSE in
	1)
		./keyboard_fr.sh
		exec $0
		;;
	2)
		./adduser.sh
		exec $0
		;;
	3) 	./deluser_pi.sh
		exec $0
		;;
	4)
		./wpa_supplicant.sh
		exec $0
		;;
	5)
		./LCD-show.sh
		exec $0
		;;
	6)
		./PiJuice.sh
		exec $0
		;;
	7)
		./SenseHat.sh
		exec $0
		;;
	8)
		raspi-config
		exec $0
		;;
	9)
		echo -e "Fin du programme.\n"
		sleep 1	
		;;
	*)
		echo -e "Mauvaise réponse ! Same player, shoot again!\n"
		exec $0
		sleep 1
		;;
esac
	
	

