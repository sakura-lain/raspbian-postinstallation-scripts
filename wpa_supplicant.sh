#! /bin/bash
echo -e "\n Sauvegarde de /etc/wpa_supplicant.conf..."
sleep 1
cp /etc/wpa_supplicant/wpa_supplicant.conf /etc/wpa_supplicant/wpa_supplicant.conf.bak && echo "Le fichier /etc/wpa_supplicant.conf original a été correctement sauvegardé"
sleep 1
echo "\nCopie du fichier wpa_supplicant.conf pré-configuré..."
sleep 1
cp wpa_supplicant.conf /etc/wpa_supplicant/wpa_supplicant.conf && echo "Le Wi-Fi a été correctement configuré."
sleep 1
