#! /bin/bash

echo "\nInstallation du SenseHat..."
sleep 1
apt install -y sense-hat
echo -e "Le SenseHat a été correctement installé.\nUn redémarrage peut être nécessaire pour que les changements prennent effet."
sleep 1
